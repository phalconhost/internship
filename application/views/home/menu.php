<ul class="tm-nav uk-nav" data-uk-nav="">
	<li class="<?php echo $this->helper_lib->getActiveMainMenu('');?>">
		<a href="<?php echo base_url(''); ?>"><span class="uk-icon-home"></span> หน้าหลัก</a>
	</li>
	<!-- 
	<li class="<?php echo $this->helper_lib->getActiveMainMenu('about');?>">
		<a href="<?php echo base_url('about/'); ?>"><span class="uk-icon-question-circle"></span> เกี่ยวกับเรา</a>
	</li>
	 -->
	<li class="<?php echo $this->helper_lib->getActiveMainMenu('contact');?>">
		<a href="<?php echo base_url('contact/'); ?>"><span class="uk-icon-comment-o"></span> ติดต่อเรา</a>
	</li>
	<li class="<?php echo $this->helper_lib->getActiveMainMenu('help');?>">
		<a href="<?php echo base_url('help/'); ?>"><span class="uk-icon-book"></span> คู่มือการใช้งาน<br/>(สำหรับนักศึกษา)</a>
	</li>
</ul>

<br/>
<div class="uk-text-left">
	<a target="_blank" href="https://web.facebook.com/groups/193312214373504/"><img src="/assets/imgs/facebook-group-join.jpg"></a>
</div>
<div class="uk-text-left">
	<img src="/assets/imgs/line_group.jpg">
</div>
